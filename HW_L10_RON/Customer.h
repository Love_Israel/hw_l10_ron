#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(std::string);
	Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item item);//add item to the set
	void removeItem(Item item);//remove item from the set

	void prtItems();
	std::string getName() { return _name; }
	const Item& getItemByNum(int num); 

private:
	std::string _name;
	std::set<Item> _items;


};
