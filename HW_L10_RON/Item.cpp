#include "Item.h"

Item :: Item (std::string name, std::string number , double price)
{
	_unitPrice = price;
	_name = name;
	_serialNumber = number;
	_count = 1;
}

Item :: ~Item () 
{
	//nothing
}

double Item :: totalPrice () const 
{
	return _unitPrice * _count;
}

bool Item :: operator< (const Item& other) const 
{
	return (_serialNumber < other.getNumber());
}

bool Item :: operator> (const Item& other) const
{
	return (_serialNumber > other.getNumber());
}

bool Item :: operator==(const Item& other) const 
{
	return (_serialNumber == other.getNumber());
}

std::string Item::getNumber() const  { return _serialNumber; };


